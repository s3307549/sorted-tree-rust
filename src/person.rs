use std::cmp::Ordering;
use std::fmt;
use std::fmt::Display;

pub struct Person {
    age: i32,
    name: String
}

impl Person {
    pub fn new(age: i32, name: String) -> Person {
        Person{age, name}
    }
}

impl Display for Person {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{{\"{}:{}\"}}", self.age, self.name)
    }
}

impl Eq for Person {}

impl PartialEq<Self> for Person {
    fn eq(&self, other: &Self) -> bool {
        self.age == other.age && self.name == other.name
    }
}

impl PartialOrd<Self> for Person {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        if self.age == other.age {
            Some(self.name.cmp(&other.name))
        } else {
            Some(self.age.cmp(&other.age))
        }
    }
}

impl Ord for Person {
    fn cmp(&self, other: &Self) -> Ordering {
        if self.age == other.age {
            self.name.cmp(&other.name)
        } else {
            self.age.cmp(&other.age)
        }
    }
}