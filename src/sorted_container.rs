use std::fmt;
use std::fmt::{Display, Formatter};

pub struct SortedContainer<T> {
    root: Option<Box<TreeNode<T>>>,
}
impl <T: Ord + Display> Display for SortedContainer<T> {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self.root {
            Some(ref root) => write!(f, "{}", root),
            None => write!(f, "Null"),
        }
    }
}

impl <T: Ord + Display> SortedContainer<T> {
    pub fn new() -> Self {
        SortedContainer {
            root: None,
        }
    }

    pub fn insert(&mut self, data: T) {
        match self.root {
            Some(ref mut root) => root.insert(data),
            None => self.root = Some(Box::new(TreeNode::new(data))),
        }
    }

    pub fn contains(&self, data: &T) -> bool {
        match self.root {
            Some(ref root) => root.contains(data),
            None => false,
        }
    }

    pub fn reset(&mut self) {
        self.root = None
    }


    pub fn erase(&mut self, data: &T) {
        if let Some(root) = self.root.take() {
            self.root = TreeNode::erase(root, data);
        }
    }
}



struct TreeNode<T> {
    data: T,
    left: Option<Box<TreeNode<T>>>,
    right: Option<Box<TreeNode<T>>>,
}

impl <T: Ord + Display> TreeNode<T> {
    pub fn new(data: T) -> Self {
        TreeNode {
            data,
            left: None,
            right: None,
        }
    }

    pub fn insert(&mut self, data: T) {
        if data < self.data {
            match self.left {
                Some(ref mut left) => left.insert(data),
                None => self.left = Some(Box::new(TreeNode::new(data))),
            }
        } else {
            match self.right {
                Some(ref mut right) => right.insert(data),
                None => self.right = Some(Box::new(TreeNode::new(data))),
            }
        }
    }

    pub fn contains(&self, data: &T) -> bool {
        if data == &self.data {
            true
        } else if data < &self.data {
            match self.left {
                Some(ref left) => left.contains(data),
                None => false,
            }
        } else {
            match self.right {
                Some(ref right) => right.contains(data),
                None => false,
            }
        }
    }
    //erase function
    fn erase(mut this: Box<TreeNode<T>>, targetdata: &T) -> Option<Box<TreeNode<T>>> {
        if targetdata < &this.data { //if the data to remove is less than the data at the current node, try again with the child as new node
            if let Some(left) = this.left.take() {
                this.left = Self::erase(left, targetdata);
            }
            return Some(this);
        }

        if targetdata > &this.data { //same as above but for right side
            if let Some(right) = this.right.take() {
                this.right = Self::erase(right, targetdata);
            }
            return Some(this);
        }

        match (this.left.take(), this.right.take()) { //correct node is found, need to check if the node to be erased has any childern
            (None, None) => None,
            (Some(left), None) => Some(left),
            (None, Some(right)) => Some(right),
            (Some(mut left), Some(right)) => {
                if let Some(mut rightmostchild) = left.rightmost_child() {
                    rightmostchild.left = Some(left);
                    rightmostchild.right = Some(right);
                    Some(rightmostchild)
                } else {
                    left.right = Some(right);
                    Some(left)
                }
            }
        }
        
    }
    //function to find the rightmost child if the node to be erased has two childern, in order to correctly reorder the tree
    pub fn rightmost_child(&mut self) -> Option<Box<TreeNode<T>>> {
        match self.right {
            Some(ref mut right) => {
                if let Some(t) = right.rightmost_child() {
                    Some(t)
                } else {
                    let mut r = self.right.take();
                    if let Some(ref mut r) = r {
                        self.right = std::mem::replace(&mut r.left, None);
                    }
                    r
                }
            },
            None => None,
        }
    }
}

impl <T: Ord + Display> Display for TreeNode<T> {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "[{},", self.data)?;
        match self.left {
            Some(ref left) => write!(f, "{} ", left),
            None => write!(f, "Null,"),
        }?;
        match self.right {
            Some(ref right) => write!(f, "{} ", right),
            None => write!(f, "Null]"),
        }
    }
}